import { get } from "axios";
import { BASE_URL } from "./apiUtils";

export function getStepsByRecipeId(recipeId) {
    return get(`${BASE_URL}/api/recipes/${recipeId}/steps`);
}