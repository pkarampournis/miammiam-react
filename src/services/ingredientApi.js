import { get } from "axios";
import { BASE_URL } from "./apiUtils";

export function getIngredientsByRecipeId(recipeId) {
    return get(`${BASE_URL}/api/recipes/${recipeId}/ingredients`)
}