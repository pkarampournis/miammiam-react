export function groupBySection(listWithSection) {
    return listWithSection.reduce((r, a) => {
        r[a.section] = r[a.section] || [];
        r[a.section].push(a);
        return r;
    }, {});
}