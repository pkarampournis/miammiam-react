const { groupBySection } = require("../sectionUtils");

describe("grouping ingredients or steps by section", () => {
    it("should return list of objects by section, in order of the section appearance", () => {
        const objectListWithSection = [
            { section: null, id: 1 },
            { section: 'a', id: 2 },
            { section: null, id: 3 },
            { section: 'b', id: 4 },
            { section: 'a', id: 5 }
        ]
        const groupedBySection = groupBySection(objectListWithSection);
        const orderedSections = ['null', 'a', 'b'];
        expect(Object.keys(groupedBySection)).toStrictEqual(orderedSections);

        expect(groupedBySection.null.map(obj => obj.id)).toStrictEqual([1, 3]);
        expect(groupedBySection.a.map(obj => obj.id)).toStrictEqual([2, 5]);
        expect(groupedBySection.b.map(obj => obj.id)).toStrictEqual([4]);
    });

});