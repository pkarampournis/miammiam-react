const { getIngredientQuantity } = require("../quantityUtils");

describe("Testing ingredient quantity related transformations", () => {
    it("should return quantity as-is when quantity has no decimals", () => {
        const ingredient = { quantity: 2 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("2");
    });

    it("should return ⅛ when quantity is close to 1/8", () => {
        const ingredient = { quantity: 0.125 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("⅛");
    });

    it("should return ⅓ when quantity is close to 1/3", () => {
        const ingredient = { quantity: 0.33 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("⅓");
    });

    it("should return ¼ when quantity is close to 1/4", () => {
        const ingredient = { quantity: 0.25 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("¼");
    });

    it("should return ½ when quantity is close to 1/2", () => {
        const ingredient = { quantity: 0.5 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("½");
    });

    it("should return ⅜ when quantity is close to 3/8", () => {
        const ingredient = { quantity: 0.375 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("⅜");
    });

    it("should return ⅝ when quantity is close to 5/8", () => {
        const ingredient = { quantity: 0.625 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("⅝");
    });

    it("should return ⅞ when quantity is close to 7/8", () => {
        const ingredient = { quantity: 0.875 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("⅞");
    });

    it("should return ⅔ when quantity is close to 2/3", () => {
        const ingredient = { quantity: 0.66 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("⅔");
    });

    it("should return ¾ when quantity is close to 3/4", () => {
        const ingredient = { quantity: 0.75 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("¾");
    });

    it("should return 1 when quantity is almost to 1", () => {
        const ingredient = { quantity: 0.99 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("1");
    });

    it("should return 3 ½ when quantity is close to 3 1/2", () => {
        const ingredient = { quantity: 3.5 };
        const textualQuantity = getIngredientQuantity(ingredient);

        expect(textualQuantity).toBe("3½");
    });

});